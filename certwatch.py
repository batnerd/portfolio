#!/usr/bin/env python3
"""Tool that takes a list of hostname:port pairs, and returns warnings
on certificates that will expire within the designated time period in days.

"""
import argparse
import datetime
import logging
import socket
import ssl


def parse_hostname_file(filepath):
    hostpairs = []
    with open(filepath, "r") as infile:
        for line in infile.readlines():
            hostpairs.append(line.strip("\r\n"))
    return hostpairs


def convert_crtdate_to_datetime(indate: str) -> datetime:
    format = "%b %d %H:%M:%S %Y %Z"
    return datetime.datetime.strptime(indate, format)


def is_about_to_expire(cert_exp_datetime: datetime, days: int) -> bool:
    log.debug(f"cert_exp_datetime={cert_exp_datetime}, days={days}")
    today = datetime.datetime.now()
    if cert_exp_datetime - today < datetime.timedelta(days):
        return True
    return False


def get_certdata(host: str, port=443) -> dict:
    """Gather certificate information"""
    context = ssl.create_default_context()
    with socket.create_connection((host, port)) as sock:
        try:
            with context.wrap_socket(sock, server_hostname=host) as ssock:
                cert = ssock.getpeercert()
                return cert
        except:
            log.error(f"Unable to connect to {host} on {port}.")
            raise


def main(args):
    rcode = 0
    hostnames = []

    if args.hostfile:
        hostnames = parse_hostname_file(args.hostfile)
    for hostname in args.hostnames:
        hostnames.append(hostname)

    for hostname in hostnames:
        log.info(f"Processing {hostname}")
        hostdata = hostname.split(":")
        # Assume default ssl port if not given
        if len(hostdata) == 1:
            hostdata.append(443)
        host, port = hostdata
        certdata = get_certdata(host, port)
        certdate = certdata["notAfter"]

        if certdata is None:
            log.error(f"No certificate data for {hostname}")
            rcode = 1
            continue
        if is_about_to_expire(convert_crtdate_to_datetime(certdate), args.days):
            print(f"{host} certificate expires on {certdate}")
            rcode = 2
    return rcode


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        """Tool to take a list of hostname:sockets and retrieve the expiration
        dates of the certificates. If the expiration date is within the 'days'
        period, warn and set the return code to 2 for an orchestration system 
        to use as a trigger to alert sysadmins for intervention.

        If a socket is not provided for a hostname, assumes 443.

        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "hostnames",
        nargs="*",
        help="List of hostname:port pairs to check, can be ignored if choosing to use the --hostfile option.",
    )
    parser.add_argument(
        "--days",
        type=int,
        default=90,
        help="Number of days lead-time before the certificate expires to warn on.",
    )
    parser.add_argument(
        "--hostfile",
        help="Path to a hostfile of line separated hostnames to check certificates, so that a large number of hostname:port pairs can be processed.",
    )
    parser.add_argument(
        "-l",
        "--log-level",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
        type=str.upper,
        default="WARNING",
        help="Include logging for everything up to the level selected",
    )
    args = parser.parse_args()

    log_format = "%(asctime)s:%(levelname)s:%(funcName)s:Line[%(lineno)d]:%(message)s"
    logging.basicConfig(format=log_format)
    log = logging.getLogger(__name__)
    log.setLevel(getattr(logging, args.log_level))

    log.debug(f"Variables used={vars(args)}")
    main(args)
