"""Tool for providing a controlled sustained transfer of files from
one directory to another

"""
import argparse
import logging
import os
import queue
import shutil
import sys
import time


class Feeder:
    def __init__(self, sourcedir, destdir):
        self.sourcedir = sourcedir
        self.destdir = destdir
        self.sourcefiles = queue.Queue()
        return None

    def delay_feed(self, qsize, delay):
        while len(os.listdir(self.sourcedir)) > 0:
            for item in os.listdir(self.sourcedir)[:qsize]:
                sourcepath = os.path.join(self.sourcedir, item)
                destpath = os.path.join(self.destdir, item)
                try:
                    shutil.move(sourcepath, destpath)
                    log.info(f"Moved {item} to {destpath}")
                except:
                    log.error(f"Unable to move source file: {sourcepath}")
                    raise
            time.sleep(delay)
        return None

    def fast_feed(self, qsize, delay):
        """Get a list of eligible filepaths from the source directory
        and move them over, keeping them at the qsize queue size to
        avoid execessive delays in processing.

        """
        for item in os.listdir(self.sourcedir):
            fullpath = os.path.join(self.sourcedir, item)
            if os.path.isfile(fullpath):
                self.sourcefiles.put(fullpath)

        while not self.sourcefiles.empty():
            available_slots = qsize - len(
                os.listdir(self.destdir)
            )  # Rework this if target can be a tree
            if available_slots > 0:
                log.debug(f"{available_slots} items to move")
                for i in range(available_slots):
                    try:
                        srcpath = self.sourcefiles.get(timeout=0.1)
                        fname = os.path.split(srcpath)[1]
                        dstpath = os.path.join(self.destdir, fname)
                        try:
                            shutil.move(srcpath, dstpath)
                            log.info(dstpath)
                        except:
                            log.error("Unable to move file : " + srcpath)
                            raise
                    except queue.Empty:
                        sys.exit(0)
                    except:
                        log.error(f"There was a problem with data in the queue")
                        raise
            else:
                time.sleep(delay)  # debug
        return None


def main(args):
    rcode = 0
    feeder = Feeder(args.indir, args.outdir)
    if args.mode == "fast-feed":
        feeder.fast_feed(args.qsize, args.delay)
    elif args.mode == "delay-feed":
        feeder.delay_feed(args.qsize, args.delay)
    else:
        raise
    return rcode


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        """This script will take all of the files in a source directory and move them
    to an output directory at a controlled rate. It was designed to solve an issue we had where a 
    sweep process was IO bound, and if we queued up several thousand items it would cause 
    delays for other document types.

    Modes:
        [fast-feed]
            For the "Fast Feed" mode, this tool will move files to a destination directory not
            to exceed the max queue size that is defined. This mode is primarily used to keep
            a continuous feed of items moved without excessive delays in destination file
            processing for folders where other processes also dump files. If you know the maximum
            speed at which the files are processed, you can calculate a predetermined delay
            introduced by your que size.
        [delay-feed]
            For the "Delay Feed" mode, this tool will move files to a destination directory
            at a steady rate, allowing for a specified delay between move attempts. This mode
            is primarily used to keep a steady feed of items without "overloading" the processor
            of the destination directory. Helps in instances where an app or service watches a directory
            and can crash or get overloaded if there are 'too many' files to process within a limited
            amount of time.
    
    """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("indir", help="Input directory to sweep from")
    parser.add_argument("outdir", help="Output directory to place files into")
    parser.add_argument(
        "--mode",
        choices=["fast-feed", "delay-feed"],
        default="fast-feed",
        help="Select the method you want to use to transfer files.",
    )
    parser.add_argument(
        "-q",
        "--qsize",
        type=int,
        default=50,
        help="For Fast-feed this is the max items allowed in a destination path a once, for Delayed-feed this is the amount of items to copy per cycle",
    )
    parser.add_argument(
        "--delay",
        type=int,
        default=60,
        help="Set the time in seconds between file sweeps. For Fast-feed, this is the delay between max queue count checks. For Delayed-feed this is the time between another copy cycle",
    )
    parser.add_argument(
        "--log-level",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
        type=str.upper,
        default="WARNING",
        help="Set the logging level",
    )
    args = parser.parse_args()

    log_format = "%(asctime)s:%(levelname)s:%(funcName)s:Line[%(lineno)d]:%(message)s"
    logging.basicConfig(format=log_format)
    log = logging.getLogger(__name__)
    log.setLevel(getattr(logging, args.log_level))

    log.debug(f"Variables used={vars(args)}")
    main(args)
