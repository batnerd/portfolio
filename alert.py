#!/usr/bin/env python3
"""Tool to send communication on exit code from program execution.

"""
import http.client
import json
import sys
import urllib.parse

from abc import ABC, abstractmethod


class MessageBroker(ABC):
    @abstractmethod
    def send_message(self, title: str, message: str) -> None: ...


class BrokerPushover(MessageBroker):
    def __init__(self, userkey: str, apitoken: str) -> None:
        self.userkey = userkey
        self.apitoken = apitoken
        pass

    def send_message(self, title: str, message: str) -> None:
        payload = {
            "token": self.apitoken,
            "user": self.userkey,
            "title": title,
            "message": message,
        }
        conn = http.client.HTTPSConnection("api.pushover.net:443")
        conn.request(
            "POST",
            "/1/messages.json",
            urllib.parse.urlencode(payload),
            {"Content-type": "application/x-www-form-urlencoded"},
        )
        conn.getresponse()


class BrokerGotify(MessageBroker):
    def __init__(self, host: str, apptoken: str) -> None:
        self.hostname = host
        self.path = f"/message?token={apptoken}"
        return None

    def send_message(self, title: str, message: str) -> None:
        payload = json.dumps(
            {
                "message": message,
                "priority": 5,
                "title": title,
            }
        )
        # Only allowing non https currently
        conn = http.client.HTTPConnection(f"{self.hostname}:80")
        conn.request(
            "POST",
            url=self.path,
            body=payload,
            headers={
                "Content-type": "application/json",
            },
        )
        conn.getresponse()


def main(args) -> None:
    if not args.message:
        args.message = sys.stdin.read()
    match args.provider:
        case "pushover":
            broker = BrokerPushover(
                userkey=args.userid,
                apitoken=args.apitoken,
            )
        case "gotify":
            broker = BrokerGotify(
                host=args.host,
                apptoken=args.apitoken,
            )
    broker.send_message(args.title, args.message)
    return None


if __name__ == "__main__":
    import argparse
    import logging

    parser = argparse.ArgumentParser(
        """Notification tool, whose primary design is to alert an admin 
        on completion of a job. Can either take stdin, or given messages.
            
            """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--title",
        help="String to use in the notificaton title area",
        default="Notification!",
    )
    parser.add_argument(
        "--message",
        help="String to use as the body of the notification if not using stdin",
    )
    parser.add_argument("--userid", help="Username/userkey info for provider usage.")
    parser.add_argument(
        "--apitoken",
        help="API or Application token used interchangably for different providers",
    )
    parser.add_argument(
        "--host",
        help="Hostname or ip of the server for providers that are not hardcoded or self-hosted",
    )
    parser.add_argument(
        "--provider",
        help="Messaging provider to use.",
        choices=["pushover", "gotify"],
        default="pushover",
        type=str.lower,
    )
    parser.add_argument(
        "-l",
        "--log-level",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
        type=str.upper,
        default="WARNING",
        help="Include logging for everything up to the level selected",
    )
    args = parser.parse_args()

    log_format = "%(asctime)s:%(levelname)s:%(funcName)s:Line[%(lineno)d]:%(message)s"
    logging.basicConfig(format=log_format)
    log = logging.getLogger(__name__)
    log.setLevel(getattr(logging, args.log_level))

    log.debug(f"Variables used={vars(args)}")
    main(args)
