#!/usr/bin/env python3
"""Module to purge files/directories that have passed a retention period

"""
import argparse
import datetime
import errno
import fnmatch
import logging
import os


def list_eligible_files(path, retention, pattern, mode):
    for root, subdirs, files in os.walk(path):
        for file_ in files:
            filename = os.path.join(root, file_)
            if is_expired(filename, retention, mode) and fnmatch.fnmatch(
                filename, pattern
            ):
                yield filename


def is_expired(inFile, retention, mode):
    """Bool True if file is older than retention.

    Method uses created(c), modified(m), or accessed(a) file stamps to
    determine retention. Defaults to last modified.

    """
    today = datetime.date.today()
    if mode == "m":
        filedate = datetime.date.fromtimestamp(os.path.getmtime(inFile))
    elif mode == "c":
        filedate = datetime.date.fromtimestamp(os.path.getctime(inFile))
    elif mode == "a":
        filedate = datetime.date.fromtimestamp(os.path.getatime(inFile))

    if today - filedate > datetime.timedelta(retention):
        return True
    else:
        return False


def main(args):
    rcode = 0
    bytes_freed = 0
    if args.dry_run:
        for path in args.pathnames:
            for pathname in list_eligible_files(
                path, args.retention, args.pattern, args.mode
            ):
                bytes_freed += os.path.getsize(pathname)
                print(f"Eligible file: {pathname}")
        print(f"Total Gb that would be freed: {bytes_freed / (1024 ** 3)}")
        return rcode

    for path in args.pathnames:
        for pathname in list_eligible_files(
            path, args.retention, args.pattern, args.mode
        ):
            try:
                bytes_freed += os.path.getsize(pathname)
                os.remove(pathname)
                print(f"Removed: {pathname}")
            except:
                log.error(f"Unable to remove {pathname}")
                rcode = 1

    if not args.preserve_folders:
        for path in args.pathnames:
            for root, subdirs, files in os.walk(path, topdown=False):
                for subdir in subdirs:
                    foldername = os.path.join(root, subdir)
                    try:
                        os.rmdir(foldername)
                    except OSError as error:
                        if error.errno == errno.ENOTEMPTY:
                            log.warning(
                                f"Unable to remove non-empty directory, skipping {foldername}"
                            )

    print(f"Total Gb freed {bytes_freed / (1024 ** 3)}")
    return rcode


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""Module to purge files from a directory
    or set of directories. WARNING!!: This does no symlink checking, and if you
    have a symlink to another directory in your target, it _might_ jump out and
    purge the linked file. Make sure that you know what you are doing when
    you set the target.
    
    """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("retention", type=int, help="Number of days to expire after")
    parser.add_argument(
        "pathnames", nargs="+", help="Full paths to directories to process"
    )
    parser.add_argument(
        "-m",
        "--mode",
        default="m",
        help="Choose the criteria for expiration on file metadata.. (c)reation date, (m)odify date, (a)ccessed date",
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        help="Do a dry run, only printing the eligible files for verification before running live on production servers",
    )
    parser.add_argument(
        "-p",
        "--pattern",
        default="*",
        help="Only purge files that match the given fnmatch pattern.",
    )
    parser.add_argument(
        "--preserve-folders",
        action="store_true",
        default=False,
        help="Preserve the folder structure by not deleting empty folders.",
    )
    parser.add_argument(
        "-l",
        "--log-level",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
        type=str.upper,
        default="WARNING",
        help="Include logging for everything up to the level selected",
    )
    args = parser.parse_args()

    log_format = "%(asctime)s:%(levelname)s:%(funcName)s:Line[%(lineno)d]:%(message)s"
    logging.basicConfig(format=log_format)
    log = logging.getLogger(__name__)
    log.setLevel(getattr(logging, args.log_level))

    log.debug(f"Variables used={vars(args)}")
    main(args)
