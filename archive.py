"""Recreation of a tool I used in the financial world to organize and
categorize document storage into a repo for later processing by a much
larger "cold" indexing/search database.

Note that it goes by individual file dates, so files in the same directory
with differing dates might end up in completely different archives. This
tool was designed to move logs and other procedurally generated files,
not storage for "packages" of files or apps that might have a wide range
of file dates within the folder, that would need to be kept together.

"Caveat Emptor"

"""
import argparse
import datetime
import logging
import os
import re
import time
import zipfile


def parse_input_file(filepath):
    filepaths = []
    with open(filepath, "r") as infile:
        for pathname in infile.readlines():
            log.debug(pathname)
            filepaths.append(pathname.strip())
    return filepaths


def list_filenames(inpaths):
    """Take a list of input paths and parse throug them, identifying
    valid full filepaths.

    """
    filepaths = []
    for pathname in inpaths:
        if os.path.isfile(pathname):
            filepaths.append(pathname)
        elif os.path.isdir(pathname):
            for root, subdirs, files in os.walk(pathname):
                for file in files:
                    fullpath = os.path.join(root, file)
                    filepaths.append(fullpath)
    return filepaths


def filter_filepaths(inpaths, retention, mode, filter=None, invert=False):
    eligible_filepaths = []
    if filter:
        filtered_paths = []
        for pathname in inpaths:
            if invert:
                if not re.match(filter, pathname):
                    filtered_paths.append(pathname)
            else:
                if re.match(filter, pathname):
                    filtered_paths.append(pathname)
        inpaths = filtered_paths

    for pathname in inpaths:
        if is_ready(pathname, retention, mode):
            eligible_filepaths.append(pathname)
    return eligible_filepaths


def is_ready(filename, retention, mode):
    """Check to see the modification date on the file, and if it is over the
    retention period, return True to calling archive function.

    """
    if mode == "m":
        filedate = datetime.date.fromtimestamp(os.path.getmtime(filename))
    elif mode == "c":
        filedate = datetime.date.fromtimestamp(os.path.getctime(filename))
    elif mode == "a":
        filedate = datetime.date.fromtimestamp(os.path.getatime(filename))

    now = datetime.date.today()
    if filedate < (now - datetime.timedelta(retention)):
        return True
    else:
        return False


def archive(filename, outdir, mode):
    """Take an item and archive it to the requisite matching archive."""
    if mode == "m":
        filedate = datetime.date.fromtimestamp(os.path.getmtime(filename))
    elif mode == "c":
        filedate = datetime.date.fromtimestamp(os.path.getctime(filename))
    elif mode == "a":
        filedate = datetime.date.fromtimestamp(os.path.getatime(filename))

    arcdate = str(filedate.year) + str(filedate.month).zfill(2)
    arcname = "archive_" + arcdate + ".zip"
    archive = os.path.join(outdir, arcname)
    with zipfile.ZipFile(
        archive, "a", compression=zipfile.ZIP_DEFLATED, compresslevel=9
    ) as zfile:
        try:
            shortname, extension = os.path.splitext(filename)
            if extension.lower() != (".zip" or ".cab"):
                zfile.write(filename)
            else:
                zfile.write(filename, compress_type=zipfile.ZIP_STORED)
            log.info(f"Compressed: {filename}")
        except:
            log.error(f"Failed to compress: {filename}")
        try:
            os.remove(filename)
        except:
            log.error(f"Failed to remove {filename}")


def main(
    outdir,
    retention,
    inpaths,
    mode,
    filter=None,
    invert=False,
    preserve_folders=False,
    test=False,
):
    rcode = 0

    filepaths = list_filenames(inpaths)
    eligible_files = filter_filepaths(filepaths, retention, mode, filter, invert)

    for item in eligible_files:
        log.info(f"Archiving eligible file: {item}")
        print(item)
        if not test:
            try:
                archive(item, outdir, mode)
            except FileNotFoundError:
                log.warning(
                    f"{item} no longer found on disk, outside cleanup processes may have removed it."
                )

    if not preserve_folders:
        for item in inpaths:
            if os.path.isdir(item):
                for root, subdirs, files in os.walk(item, topdown=False):
                    for subdir in subdirs:
                        pathname = os.path.join(root, subdir)
                        if len(os.listdir(pathname)) == 0:
                            try:
                                if not test:
                                    os.rmdir(pathname)
                                log.info(f"Removed empty folder: {pathname}")
                            except:
                                log.error(f"Unable to remove: {pathname}")
                        else:
                            pass
    return rcode


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("outdir", help="Path of the output folder of the archive files")
    parser.add_argument(
        "retention",
        type=int,
        help="The retention period (in days) before the files are ok to archive",
    )
    parser.add_argument(
        "pathnames",
        nargs="*",
        help="The files and folders to process. Note that if the arg is a folder, it will grab _all_ files within that match the criteria.",
    )
    parser.add_argument(
        "--input-file",
        help="Path to a line separated input file containing pathnames to process.",
    )
    parser.add_argument(
        "-m",
        "--mode",
        choices=["c", "m", "a"],
        type=str.lower,
        default="m",
        help="File date metadata to use: (c)reated date, (m)odified date, last (a)ccessed date; during retention checking. Note many unix/linux filesystems do not store create time, and it is last change time only.",
    )
    parser.add_argument(
        "-p",
        "--preserve-folders",
        action="store_true",
        help="Do not delete empty folders left behind in the archive process",
    )
    parser.add_argument(
        "-f",
        "--filter",
        help="Filter pathnames to only items that match your regex pattern",
    )
    parser.add_argument(
        "-i",
        "--invert",
        action="store_true",
        help="Invert the matching of the --filter to items that DO NOT match your regex pattern. No effect without a filter, does NOT affect retention period",
    )
    parser.add_argument(
        "-t",
        "--test",
        action="store_true",
        help="Run in test mode, where it will only print out eligible items that would have been archived using the settings, but will not actually archive anything",
    )
    parser.add_argument(
        "-l",
        "--log-level",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
        type=str.upper,
        default="WARNING",
        help="Include logging for everything up to the level selected",
    )
    args = parser.parse_args()

    logging.basicConfig()
    log = logging.getLogger("ArchiveTool")
    log.setLevel(getattr(logging, args.log_level))

    if args.input_file:
        args.pathnames = args.pathnames + parse_input_file(args.input_file)

    starttime = time.perf_counter()
    log.debug(args)
    main(
        args.outdir,
        args.retention,
        args.pathnames,
        args.mode,
        args.filter,
        args.invert,
        args.preserve_folders,
        args.test,
    )
    endtime = time.perf_counter()
    log.info(f"Runtime: {endtime - starttime} seconds")
