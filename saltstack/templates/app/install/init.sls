# ==============================================================================
# APPLICATION STATE
# ==============================================================================

# Configuration state for variables and settings that other unrelated states
# might rely on as being consistent (opening firewall for security state, etc)

# Use the map.jinja configuration file to set up new
# apps with consistency and minimal effort.

# States applied 'in order'
include:
    - .prerequisites
    - .environment
    - .run
    - .configuration
    - .cleanup
