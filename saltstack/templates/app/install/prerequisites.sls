# Import map variables
{% from "../map.jinja" import app with context %}


# Install prerequisite repo packages if necessary ahead of the install
{% if app.prerequisite_packages|length > 0 %}
{{ sls_path }}_prerequisites:
    pkg.installed:
        - pkgs: {{ app.prerequisite_packages }}
        - refresh: True
{% endif %}

