# Import map variables
{% from "../map.jinja" import app with context %}


# Use this to do 'post-install' configuration items that each app should be
# responsible for mainaining it's own.. such as backups, apparmor settings, etc

# ==============================================================================
# BACKUPS
# ==============================================================================

# If the program generates data for backup, it is set up here
{% if (app.backup_paths or app.backup_sql)|length > 0 %}
{{ sls_path }}_backup_location:
    file.directory:
        - name: /backups
        - user: root
        - group: root
        - mode: 770
        - makedirs: true

{{ sls_path }}_backup_files:
    file.managed:
        - names:
            - /opt/scripts/backup.py:
                - source: salt://scripts/backup.py
            - /etc/opt/scripts/{{ sls_path }}.mf:
                - source: salt://{{ slspath }}/template.mf
        - user: root
        - group: root
        - mode: 770
        - makedirs: true
        - template: jinja
        - context:
            app: {{ app|tojson }}

{{ sls_path }}_backup_crontab:
    cron.present:
        - name: /opt/scripts/backup.py --create /etc/opt/scripts/{{ app.name }}.mf
        - user: root
        - minute: 00
        - hour: 04
        - daymonth: '*'
        - month: '*'
        - dayweek: '*'
{% endif %}


# ==============================================================================
# FIREWALL HOLE PUNCHING
# ==============================================================================
{% if not grains['virtual']|lower == "lxc" %}
{% if app.firewall_tcp|length > 0 %}
{{ sls_path }}_firewall_rule_tcp:
    iptables.append:
        - table: filter
        - chain: INPUT
        - jump: ACCEPT
        - match: state
        - connstate: NEW
        - dports: {{ app.firewall_tcp }}
        - protocol: tcp
        - save: True
        - comment: "{{ app.name }}"
{% endif %}

{% if app.firewall_udp|length > 0 %}
{{ sls_path }}_firewall_rule_udp:
    iptables.append:
        - table: filter
        - chain: INPUT
        - jump: ACCEPT
        - match: state
        - connstate: NEW
        - dports: {{ app.firewall_udp }}
        - protocol: udp
        - save: True
        - comment: "{{ app.name }}"
{% endif %}
{% endif %}
