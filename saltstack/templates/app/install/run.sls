# Import variables
{% from "../map.jinja" import app with context %}

# ==============================================================================
# DIRECT BINARY INSTALLATION
# ==============================================================================

# Install regular repo packages if available
{% if app.packages|length > 0 %}
{{ sls_path }}_app_packages:
    pkg.installed:
        - pkgs: {{ app.packages }}
        - refresh: True
{% endif %}


# Copy over bin file or unpack if archive
{% set archive_types = ["tar", "gz", "zip"] %}

{% if app.install_filename[grains['cpuarch']]|length > 0 %}
{% set file_ext = app.install_filename[grains['cpuarch']].split('.')[-1] %}
{% if file_ext in archive_types %}
{{ sls_path }}_extract_archive:
    archive.extracted:
        - enforce_toplevel: False
        - name: {{ app.install_dir }}
        - source: salt://{{ slspath }}/files/{{ app.install_filename[grains['cpuarch']] }}
#        - makedirs: True
#        - clean_parent: True
{% else %}
{{ sls_path }}_bin:
    file.managed:
        - name: {{ app.install_dir }}/{{ app.install_filename[grains['cpuarch']] }}
        - source: salt://{{ slspath }}/files/{{ app.install_filename[grains['cpuarch']] }}
        - mode: 755
        - makedirs: True
{%- endif %}
{%- endif %}

# Copy the default config file if it exists
{% if app.config_name|length > 0 %}
{{ sls_path }}_config_file:
    file.managed:
        - name: {{ app.install_dir}}/{{ app.config_name }}
        - source: salt://{{ slspath }}/files/{{ app.config_name }}
        - mode: 644
{% endif %}


# Install the configured service file if the app would benefit from it
{% if app.service_name|length > 0 %}
{{ sls_path }}_systemd_service:
    file.managed:
        - name: /etc/systemd/system/{{ app.service_name }}.service
        - source: salt://{{ slspath }}/template.service
        - mode: 644
        - template: jinja
        - context:
            app: {{ app|tojson }}

{{ sls_path }}_start_services:
    service.running:
        - name: {{ app.service_name }}
        - enable: True
        {%- if app.config_name|length > 0 %} # Only watch if a config file was supplied
        - watch:
            - file: {{ app.install_dir}}/{{ app.config_name }}
        {% endif %}
{% endif %}


