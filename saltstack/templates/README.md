## Saltstack Templating Project

The architecture of this project, is to encapsulate the applications in a way where I can 
layer / compose them together without conflict, and / or move them between 
machines, containers, etc. seamlessly.

These templates are the work around getting a sysadmin "generic" way of 
app / role deployments, so that the following is always met for every app by design:

- install and build folders, where the application can be set up, no matter how 
    complicated the build process. Should include service and configuration states 
    that are required to get it up and running 'by default' and should not include 
    site specific settings/configs. That should be layered on outside at the site 
    level states. This state focuses on getting a "default" installation going. From 
    there, site and restore states can be run to put everything back together.
- upgrade.sls, where all necessary steps are included to upgrade the app to the 
    latest versions
- backup.sls, where all app configurations and data are packaged. Should also include
    configuration of auto-backups to be default for every app.
- restore.sls, where I can 'one click run' and have the backup package restored from 
    it's configured location. The main goal here is to have the capability for 
    disaster recovery, or migrations to be as seamless as possible.
