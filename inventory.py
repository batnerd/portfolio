#!/usr/bin/env python3
"""Stand alone inventory module from my switchblade framework, used for
gathering host and networking data.


"""
import argparse
import csv
import ctypes
import os
import queue
import re
import subprocess
import sys
import threading
import time


_HOSTS = []
_DONE_HOSTS = []


class DomainInfo:
    def __init__(self, domains):
        self.zone = []  # Domain under the tld
        self.nameservers = {}  # Whois nameserver lists tied to domain keys
        self.subnets = []  # Subnet data visible to scans
        self.certs = []  # Certificates used for TLS/SSL
        self.whois = {}  # Registrar and domain user information
        self.wanips = []  # List of public IP addresses gleaned from a scan

    def get_whois_data(self):
        """Gather whois data about the given domains.

        Include 'common' domain checks for things that the operator might
        not have thought to check, such as www. and MX records

        """
        # Check the following regardless if the operator called for them
        # return the data for anything listed
        common_domains = ["www", "vpn", "mail", "autodiscover", "support"]
        raise NotImplemented

    def parse_whois_data(self):
        """Parse the whois data into a dict of items that can be passed
        to reporting or documentation

        """
        raise NotImplemented

    def get_wan_ips(self):
        """Scan and parse out the WAN IPs that are visible from the scanning
        machine.

        """
        raise NotImplemented


class WindowsHost:
    def __init__(self, target):
        self.target = target
        self.rawhostdata = {}  # Raw dump from the machine, for parsing
        self.hostdata = {}
        return None

    def parse_wmic(self):
        """Parse the raw output of windows wmic tool into dicts

        Data format:
            {single value key : {key: values},
            {multivalued key: ({key: value-pairs},
                               {key: value-pairs},
                               {key: value-pairs})}

        """
        for key, value in self.rawhostdata.items():
            value = value.decode("ascii", errors="ignore").strip("\r\n")
            headers = value.split("\r\r\n")[0]
            rows = value.split("\r\r\n")[1:]
            if len(rows) == 1:
                self.hostdata[key] = dict(zip(headers.split(","), rows[0].split(",")))
            else:
                self.hostdata[key] = []
                for row in rows:
                    self.hostdata[key].append(
                        dict(zip(headers.split(","), row.split(",")))
                    )

    def get_hostdata(self):
        """Use windows wmic tool to query windows hosts for hostdata used in
        inventory control

        """
        hostkeys = ["computersystem", "bios", "cpu", "logicaldisk", "os", "nicconfig"]
        for key in hostkeys:
            # Removes commas in cmd string, or parsing fails later
            cmd = " ".join(
                [
                    "wmic",
                    "/node:" + '"' + self.target + '"',
                    key,
                    "get",
                    "/translate:nocomma",
                    "/format:csv",
                ]
            )
            stdout, stderr = subprocess.Popen(
                cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
            ).communicate()
            # Breakout on any error to avoid wasting time
            if len(stderr) != 0:
                self.rawhostdata = {}  # Reset non-responsive for parsing
                break
            self.rawhostdata[key] = stdout

        if len(self.rawhostdata) != 0:
            try:
                self.hostdata["pagefile"] = str(
                    os.path.getsize("\\\\" + self.target + "\\c$\\pagefile.sys")
                )
            except:
                self.hostdata["pagefile"] = str(0)
            try:
                self.hostdata["hiberfile"] = str(
                    os.path.getsize("\\\\" + self.target + "\\c$\\hiberfil.sys")
                )
            except:
                self.hostdata["hiberfile"] = str(0)

    def get_bitlocker_data(self):
        """Run the manage-bde tool and get the data back for parsing
        into the inventory

        """

        # Change to allow 'os installed disk' method, not all will be on C
        self.hostdata["bitlocker"] = {"C": {"Encryption Method": ""}}

        cmd = " ".join(
            [
                "%windir%\\sysnative\\manage-bde.exe",
                "-computername",
                self.target,
                "-status",
            ]
        )
        stdout, stderr = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        ).communicate()

        # Key parse and store for later use
        re_volumes = re.compile(r"(Volume.*?Protectors.*?\r\n\r\n)", re.DOTALL)
        re_volume_id = re.compile(r"Volume (\w):")
        re_keybody = re.compile(r"\r\n\r\n(.*?)\r\n\r\n", re.DOTALL)

        volumes = re.findall(re_volumes, stdout.decode("ascii", errors="ignore"))
        for volume in volumes:
            try:
                volume_id = re.search(re_volume_id, volume).group(1)
                keybody = re.search(re_keybody, volume).group(1)
                self.hostdata["bitlocker"][volume_id] = {}
                for item in keybody.splitlines():
                    # When bitlocker is active, the Key Protectors field uses
                    # newlines for each entry. This is an ugly hack that doesn't
                    # care to store the key protector values, just store the
                    # other 'valued' key/values that come before
                    try:
                        key, value = item.split(":")
                    except:
                        key, value = [item.split(":")[0], ""]
                    key = key.strip()
                    value = value.strip()
                    self.hostdata["bitlocker"][volume_id][key] = value
            except:
                print(self.target + " : BITLOCKER FAIL")
        return None


class NetworkDevice:
    def __init__(self, target):
        self.target = ""
        self.hostdata = {}
        self.binaryconfig = b""  # Used for raw binary storage from SFTP dump
        self.asciiconfig = ""  # Used for telnet switch output
        return None

    def get_raw_config(self):
        """Method that sets up and calls the SFTP for native switch backups"""
        return None

    def parse_raw_config(self):
        """Method to call appropriate device binary parsing, to return
        the values into something I can report

        """
        raise NotImplemented

    def parse_telnetdata(self):
        raise NotImplemented

    def get_telnet_config(self):
        """Log into a network device using ssh/telnet to gather
        config info as a backup (cisco output doubles as config input)

        """
        # Use the switchblade module methods of calling with separation of
        # concerns
        raise NotImplemented


class Reporting:
    def __init__(self, csvpath):
        self.outpath = csvpath
        self.header = (
            "Hostname",
            "Make",
            "Model",
            "Serial",
            "CPU Family",
            "Cores",
            "RAM",
            "DISKS (Total, Used, % Free)",
            "BitLocker Encryption",
            "OS",
            "Username",
            "Hiberfile Size",
            "Pagefile Size",
        )
        return None

    def gen_csvfile(self, hostlist):
        """Generate a csv file from a list of host dictionaries"""
        outrows = []
        if not os.path.isfile(self.outpath):
            outrows.append(self.header)

        def parse_diskdata(hostdata):
            """Parse the disk data from a single host out into a single
            field entry that can be inserted below. There is logic
            here to find all of the partitions that are fixed disk partitions
            only. I only want local fixed disks for now.

            All formatting done here as well, to keep the call simplified.

            Returns (fullsize, used space, percent free) in the format that
            can show nicely in the csv output total

            """
            if isinstance(hostdata["logicaldisk"], dict):
                logicaldisk = hostdata["logicaldisk"]
                name = logicaldisk["DeviceID"]
                total = "{0:.2f}".format(int(logicaldisk["Size"]) / 1024 ** 3) + " GiB"
                free = (
                    "{0:.2f}".format(int(logicaldisk["FreeSpace"]) / 1024 ** 3) + " GiB"
                )
                used = (
                    "{0:.2f}".format(
                        (int(logicaldisk["Size"]) - int(logicaldisk["FreeSpace"]))
                        / 1024 ** 3
                    )
                    + " GiB"
                )
                pct_free = "{0:.0f}".format(
                    int(logicaldisk["FreeSpace"]) / int(logicaldisk["Size"]) * 100
                )
                disks = (name, total, used, pct_free)

            if isinstance(hostdata["logicaldisk"], list):
                disks = []
                for logicaldisk in hostdata["logicaldisk"]:
                    if logicaldisk["DriveType"] == "3":
                        try:
                            name = logicaldisk["DeviceID"]
                            total = (
                                "{0:.2f}".format(int(logicaldisk["Size"]) / 1024 ** 3)
                                + " GiB"
                            )
                            free = (
                                "{0:.2f}".format(
                                    int(logicaldisk["FreeSpace"]) / 1024 ** 3
                                )
                                + " GiB"
                            )
                            used = (
                                "{0:.2f}".format(
                                    (
                                        int(logicaldisk["Size"])
                                        - int(logicaldisk["FreeSpace"])
                                    )
                                    / 1024 ** 3
                                )
                                + " GiB"
                            )
                            pct_free = "{0:.0f}".format(
                                int(logicaldisk["FreeSpace"])
                                / int(logicaldisk["Size"])
                                * 100
                            )
                            disks.append((name, total, used, pct_free))
                        except:
                            raise
            return disks

        # There are instances where it has multiple cpus.
        # Return CPUx:NxGhz
        def parse_cpudata(hostdata):
            """Parse the hostdata to find the processor family,
            cpu socket count, core count, and max speed.

            Return the Name, and CPU ids in the form of listed strings
                (Name, [CPUx: N x Ghz, CPUy: N x Ghz])

            """
            # Single CPU socket
            if isinstance(hostdata["cpu"], dict):
                name = hostdata["cpu"]["Name"]
                socket_id = hostdata["cpu"]["DeviceID"]
                if "NumberOfLogicalProcessors" in hostdata["cpu"]:
                    cpu_data = (
                        socket_id
                        + ": "
                        + hostdata["cpu"]["NumberOfLogicalProcessors"]
                        + " x "
                        + "{0:.2f}".format(int(hostdata["cpu"]["MaxClockSpeed"]) / 1024)
                        + " Ghz"
                    )
                else:
                    cpu_data = (
                        socket_id
                        + ": "
                        + "1 x "
                        + "{0:.2f}".format(int(hostdata["cpu"]["MaxClockSpeed"]) / 1024)
                        + " Ghz"
                    )

            # Multiple CPU sockets
            if isinstance(hostdata["cpu"], list):
                name = hostdata["cpu"][0]["Name"]
                cpu_data = []
                for item in hostdata["cpu"]:
                    socket_id = item["DeviceID"]
                    if "NumberOfLogicalProcessors" in item:
                        cpu_data.append(
                            socket_id
                            + ": "
                            + item["NumberOfLogicalProcessors"]
                            + " x "
                            + "{0:.2f}".format(int(item["MaxClockSpeed"]) / 1024)
                            + " Ghz"
                        )
                    else:
                        cpu_data.append(
                            socket_id
                            + ": "
                            + "1 x "
                            + "{0:.2f}".format(int(item["MaxClockSpeed"]) / 1024)
                            + " Ghz"
                        )
            return name, cpu_data

        for host in hostlist:
            if len(host) == 0:
                continue  # Unreachable hosts return empty dict in WindowsHost() This assumption FAILS now, since bitlocker is always added first
            try:
                outrow = [
                    host["computersystem"]["Name"],
                    host["bios"]["Manufacturer"],
                    host["computersystem"]["Model"],
                    host["bios"]["SerialNumber"],
                    parse_cpudata(host)[0],  # Processor Name
                    parse_cpudata(host)[1],  # CPU data
                    "{0:.0f}".format(
                        round(
                            int(host["computersystem"]["TotalPhysicalMemory"])
                            / 1024 ** 3
                        )
                    )
                    + " GiB",
                    parse_diskdata(host),
                    host["bitlocker"]["C"]["Encryption Method"],
                    host["os"]["Caption"],
                    host["computersystem"]["UserName"],
                    "{0:.2f}".format(round(int(host["hiberfile"]) / 1024 ** 3, 2))
                    + " GiB",
                    "{0:.2f}".format(round(int(host["pagefile"]) / 1024 ** 3, 2))
                    + " GiB",
                ]
                outrows.append(outrow)
            except:
                print("Errors written to errfile.txt in directory!")
                with open("errfile.txt", "a") as errfile:
                    errfile.write(str(host))

        with open(self.outpath, "a") as outfile:
            csvout = csv.writer(outfile, lineterminator="\n")
            csvout.writerows(outrows)


def load_hostfile(infile):
    """Take a line delimited file and load it for parsing"""
    hosts = []
    with open(infile, "r") as hostfile:
        for host in hostfile.read().splitlines():
            hosts.append(host.strip("\r"))
    return hosts


def load_csv(infile):
    """Load the existing inventory outfile into memory, so that I can
    re-run the script many times to pick up missing machines without
    creating duplicates or resetting the inventory every time to only
    machines that answer.

    """
    existing_hosts = []
    with open(infile, "r", newline="") as _infile:
        incsv = csv.reader(_infile)
        for row in incsv:
            existing_hosts.append(row[0].lower())
    return existing_hosts


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("hosts", nargs="+", help="List of hosts to return data for")
    parser.add_argument(
        "-o",
        "--outfile",
        help="Alternate csv output path. Defaults to active directory",
    )
    parser.add_argument(
        "--loop",
        help="Number of times to loop the script, to pickup less responsive or newly active machines",
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Turn on debugging. Wild west stuff, don't use unless you are the one coding it!!",
    )
    args = parser.parse_args()

    # ~if args.debug:
    # ~import sys
    # ~host = WindowsHost('localhost')
    # ~host.get_bitlocker_data()
    # ~print(host.bitlocker)
    # ~sys.exit(0)

    # MAIN_LOOP()
    _MAX_WORKERS = 30

    def worker():
        while True:
            host = _HOSTQ.get()
            w = WindowsHost(host)
            w.get_hostdata()
            w.parse_wmic()
            w.get_bitlocker_data()
            _DONEQ.put(w.hostdata)
            _HOSTQ.task_done()

    if not args.outfile:
        args.outfile = "inventory.csv"

    for host in args.hosts:
        if os.path.isfile(host):
            _HOSTS = load_hostfile(host)
        else:
            _HOSTS.append(host)

    # Main() loop, by count
    loopcount = 1
    if args.loop:
        loopcount = int(args.loop)

    for x in range(loopcount):
        start = time.time()
        _HOSTQ = queue.Queue()
        _DONEQ = queue.Queue()

        # Don't put already found hosts into processing again
        if os.path.isfile(args.outfile):
            _DONE_HOSTS = load_csv(args.outfile)
        for host in _HOSTS:
            if host.lower() not in _DONE_HOSTS:
                _HOSTQ.put(host)

        for i in range(_MAX_WORKERS):
            t = threading.Thread(target=worker)
            t.daemon = True
            t.start()
        _HOSTQ.join()

        hostlist = []
        while not _DONEQ.empty():
            hostlist.append(_DONEQ.get())

        # Generate the output for humans
        report = Reporting(args.outfile)
        report.gen_csvfile(hostlist)
        end = time.time()
        print("Last run took " + str((end - start) / 60) + " minutes.")
