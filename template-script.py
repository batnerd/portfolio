#!/usr/bin/env python3
"""[Main module description]

"""
import argparse
import logging


def main(args):
    rcode = 0
    return rcode


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        """[Main helpfile description]
            
            """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-l",
        "--log-level",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
        type=str.upper,
        default="WARNING",
        help="Include logging for everything up to the level selected",
    )
    args = parser.parse_args()

    log_format = "%(asctime)s:%(levelname)s:%(funcName)s:Line[%(lineno)d]:%(message)s"
    logging.basicConfig(format=log_format)
    log = logging.getLogger(__name__)
    log.setLevel(getattr(logging, args.log_level))

    log.debug(f"Variables used={vars(args)}")
    main(args)
