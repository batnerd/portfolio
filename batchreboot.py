#!/usr/bin/env python3
"""Tool for issuing reboots to servers in a list, spacing out the time
between them so that everything is not down at once, using 
all the hypervisor resources at once to boot.

"""
import argparse
import datetime
import re
import subprocess
import time


def reboot_win_machine(host, delay):
    """Send the shutdown command to a windows host."""
    if args.halt:
        cmd = [
            "shutdown",
            "/m",
            r"\\" + str(host),
            "/s",
            "/t",
            str(delay),
        ]
    else:
        cmd = [
            "shutdown",
            "/m",
            r"\\" + str(host),
            "/r",
            "/t",
            str(delay),
            "/d",
            "P:02:17",
        ]

    print(" ".join(cmd))
    subprocess.call(cmd)
    return None


def calc_start_delay(miltime):
    """Calculate the time "from now" in seconds using a military time
    input.

    Print the calculated time out for the user.

    """
    re_miltime = r"\d{4}"
    if not re.match(re_miltime, miltime):
        raise ValueError("Invalid time entered, expecting 4 digit military time")

    runtime = datetime.datetime.combine(
        datetime.date.today(), datetime.time(int(miltime[:2]), int(miltime[2:]))
    )
    now = datetime.datetime.now()
    delay = runtime - now
    return delay.seconds


def is_start_time(miltime):
    """Calculate the time "from now" in seconds using a military time
    input.

    """
    re_miltime = r"\d{4}"
    if not re.match(re_miltime, miltime):
        raise ValueError("Invalid time entered, expecting 4 digit military time")

    runtime = datetime.datetime.combine(
        datetime.date.today(), datetime.time(int(miltime[:2]), int(miltime[2:]))
    )
    now = datetime.datetime.now()
    if now > runtime:
        return True
    else:
        return False


def pretty_print_notice(starttime, hosts):
    """Take the hosts and print out a notice I can cut/paste into
    an email to notify everyone without having to type it out everytime

    """
    hours = int(starttime[:2])
    minutes = int(starttime[2:])
    setting = "am"
    if hours > 12:
        setting = "pm"
        hours -= 12
    user_time = ("%d:%02d" + setting) % (hours, minutes)

    print(
        "\n\nAs part of our regular maintenance schedule, starting at "
        + user_time
        + " we are going to be rebooting the following servers:\n"
    )
    for host in hosts:
        print("    - " + host)
    return None


if __name__ == "__main__":
    _DELAY = 300  # Default time in seconds between each machine (5 min)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "starttime",
        help="Start time (in military format) to begin rebooting the machines",
    )
    parser.add_argument(
        "hostnames", nargs="+", help='Hosts/IPs to reboot, "in order", first to last'
    )
    parser.add_argument(
        "--delay",
        type=int,
        help="Set the delay in minutes [Default " + str(_DELAY / 60) + " minutes]",
    )
    parser.add_argument(
        "--halt",
        action="store_true",
        help="Set the machines to shutdown and halt, rather than reboot",
    )
    parser.add_argument(
        "--debug", action="store_true", help="Activate debugging code blocks"
    )
    args = parser.parse_args()

    start_delay = calc_start_delay(args.starttime)
    print(
        "Waiting for : " + str(start_delay / 60 / 60) + " hours until rolling restarts"
    )

    # User input delay in minutes, convert to seconds for time.sleep()
    if args.delay is not None:
        _DELAY = args.delay * 60

    pretty_print_notice(args.starttime, args.hostnames)

    # main()
    while True:
        if is_start_time(args.starttime):
            for server in args.hostnames:
                print("Rebooting: " + server)
                reboot_win_machine(server, 60)
                print("Sleeping for " + str(_DELAY / 60) + " minutes\n")
                time.sleep(_DELAY)
            break
        else:
            time.sleep(2)
