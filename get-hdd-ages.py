#!/usr/bin/env python3
"""Determine the age of a HDD in years, of powered on time

Script that calls smartctl on every hard drive, then searches for and
prints out the ages so I can replace aging drives in my servers.

Depends on smartmontools in linux to get data

Returns the disk-by-id as well, so I can just plug it into a replacement
command without having to ID it further.

"""
import argparse
import os
import subprocess
import re


# Linux bash term color codes
_COLOR_R = "\033[91m"
_COLOR_Y = "\033[93m"
_COLOR_RESET = "\033[0m"


def get_disk_age(devpath):
    """Use smartctl tools to get the disk 'age'

    The power_on_hours can be used to roughly estimate the working life
    of a disk, so I can make an estimation about which disks should be
    replaced first.

    """
    age_cmd = ["smartctl", "-a", devpath]
    re_age = re.compile(r".*?Power_On_Hours.*?Old_age.*?(\d+).*?$", re.DOTALL)

    smartdata, smarterr = subprocess.Popen(
        age_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    ).communicate()
    ascii_data = smartdata.decode("ascii", errors="ignore")
    age_in_hours = re.search(re_age, ascii_data).group(1)
    return age_in_hours


def get_diskid(devpath):
    """Get the /dev/disk-by-id uuid of a dev"""
    disk_shortname = os.path.split(devpath)[1]
    id_path_cmd = " ".join(
        [
            "ls",
            "-l",
            "/dev/disk/by-id/",
            "|",
            "grep",
            "-iP",
            '"' + disk_shortname + "$" + '"',
        ]
    )
    re_disk_id = re.compile(r".*? (?:\d+:\d+| \d{4}) (.*?) +->", re.DOTALL)

    diskdata, diskerr = subprocess.Popen(
        id_path_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
    ).communicate()
    ascii_data = diskdata.decode("ascii", errors="ignore")
    disk_id_shortpath = re.search(re_disk_id, ascii_data).group(1)
    disk_by_id_path = os.path.join("/dev/disk/by-id/", disk_id_shortpath)
    return disk_by_id_path


def get_disksize(devpath):
    """Get the fdisk reported device size"""
    re_size = re.compile(
        r"Disk " + devpath + ": ([\d+\.]+ [TGMK]i?B),", re.DOTALL | re.IGNORECASE
    )
    cmd = ["fdisk", "-l", devpath]

    stdout, stderr = subprocess.Popen(
        cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    ).communicate()
    ascii_data = stdout.decode("ascii", errors="ignore")
    disksize = re.search(re_size, ascii_data).group(1)
    return disksize


def pretty_print(disk_data_dict):
    """Take the data and print it out for humans"""
    HEADER = ["DEV", "SIZE", "AGE (YEARS)", "DISK-BY-ID"]
    col_widths = "{0:15} {1:15} {2:15} {3:25}"
    print(col_widths.format(*HEADER))
    for key, value in disk_data_dict.items():
        age_in_years = int(value[0]) / 24 / 365.25
        age_formatted = "{0:.2f}".format(age_in_years)
        disksize = value[1]
        diskid = value[2]
        # Colorize the output
        if age_in_years > 4:
            print(_COLOR_R, end="")
        elif age_in_years > 3.2:
            print(_COLOR_Y, end="")
        else:
            pass
        print(col_widths.format(key, disksize, age_formatted, diskid))
        print(_COLOR_RESET, end="")


if __name__ == "__main__":

    _DISK_DATA = {}

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "devs",
        nargs="+",
        help="List of /dev/pathnames to the drives you want to age query",
    )
    args = parser.parse_args()

    for dev in args.devs:
        if dev not in _DISK_DATA:
            _DISK_DATA[dev] = [0, "", ""]

        _DISK_DATA[dev][0] = get_disk_age(dev)
        _DISK_DATA[dev][1] = get_disksize(dev)
        _DISK_DATA[dev][2] = get_diskid(dev)

    pretty_print(_DISK_DATA)
