"""Tool for gathering logs for troubleshooting research into a cohesive
package.

"""
import argparse
import configparser
import datetime
import logging
import os
import re
import subprocess
import zipfile
from itertools import chain
from multiprocessing.pool import ThreadPool


class Config:
    def __init__(self, startdate, enddate, configpath, outdir) -> None:
        config = configparser.ConfigParser()
        self.start_date = datetime.datetime.strptime(startdate, "%Y-%m-%d").date()
        self.end_date = datetime.datetime.strptime(enddate, "%Y-%m-%d").date()

        config.read(configpath)
        self.arcname, _ = os.path.splitext(os.path.split(configpath)[-1])
        self.outfile = os.path.join(
            outdir,
            self.arcname
            + "_"
            + startdate.replace("-", "")
            + "-"
            + enddate.replace("-", "")
            + ".zip",
        )
        self.hosts = list((value for key, value in config["hosts"].items()))
        self.logdirs = list((value for key, value in config["logdirs"].items()))
        self.static_files = list((value for key, value in config["files"].items()))
        self.wineventlogs = config["options"]["wineventlogs"]


class Package:
    def __init__(self, filepath) -> None:
        self.archive = filepath

    def create_archive(self, filelist):
        re_drive = r"(\\\w\$)"
        compressed_extensions = [".zip", ".gzip", ".lzma", ".7z"]
        log.info(f"Creating package: {self.archive}")
        with zipfile.ZipFile(
            self.archive, "w", compression=zipfile.ZIP_DEFLATED, compresslevel=9
        ) as zfile:
            for filepath in filelist:
                log.info(f"Compressing: {filepath}")

                # Clear UNC style path to create local style path in archive
                zip_member_name = re.sub(re_drive, "", filepath).lstrip("\\\\")

                # Don't try to compress already compressed files, for speed
                _, extension = os.path.splitext(filepath)
                try:
                    if extension in compressed_extensions:
                        zfile.write(
                            filepath, zip_member_name, compress_type=zipfile.ZIP_STORED
                        )
                    else:
                        zfile.write(filepath, zip_member_name)
                except:
                    log.error(f"Unable to archive file: {filepath}")
        return None


class Files:
    def __init__(self, startdate, enddate, logdirs, static_files) -> None:
        self.startdate = datetime.datetime.strptime(startdate, "%Y-%m-%d").date()
        self.enddate = datetime.datetime.strptime(enddate, "%Y-%m-%d").date()
        self.logdirs = logdirs
        self.static_files = static_files
        self.traverse = False
        self.winevent_logtypes = ["application", "system"]
        pass

    def list_eligible_logfiles(self, host):
        logpaths = []
        for logdir in self.logdirs:
            uncpath = os.path.join("\\\\", host, logdir.replace(":", "$"))
            try:
                if self.traverse:
                    for root, subs, files in os.walk(uncpath):
                        for file in files:
                            filepath = os.path.join(root, file)
                            if self.is_eligible(filepath, self.startdate, self.enddate):
                                logpaths.append(filepath)
                else:
                    for file in os.listdir(uncpath):
                        filepath = os.path.join(uncpath, file)
                        if self.is_eligible(filepath, self.startdate, self.enddate):
                            logpaths.append(filepath)
            except:
                log.error(f"Unable to list files in {uncpath}")
        return logpaths

    def get_winevent_logpaths(self, host):
        logpaths = []
        for logtype in self.winevent_logtypes:
            filename = logtype + ".evtx"
            default_path = os.path.join(r"Windows\System32\winevt\Logs", filename)
            uncpath = os.path.join("\\\\", host, "c$", default_path)
            if os.path.isfile(uncpath):
                log.debug(f"Adding {uncpath}")
                logpaths.append(uncpath)
            else:
                logpath = self.gen_winevent_logpath(host, logtype)
                if logpath is not None:
                    logpaths.append(logpath)
        return logpaths

    def gen_winevent_logpath(self, host, logtype):
        """Generate a wineventlog of type and return the path."""
        filename = logtype + ".evtx"
        # The outpath is always local to the target server, wevtutil behavior
        filepath = os.path.join("C:\\", filename)
        command = [
            "wevtutil",
            "epl",
            logtype,
            filepath,
            "/r:" + host,
            "/ow",
        ]
        win_shell_cmd = " ".join(command)
        try:
            # Return uncpath for gathering logic
            uncpath = os.path.join("\\\\", host, "c$", filename)
            log.debug(f"Host:{host}:Logtype: {logtype}:uncpath:{uncpath}")
            log.info(f"Generating {host} {logtype} event log")
            returncode = subprocess.call(win_shell_cmd, shell=True)
            if returncode == 0:
                return uncpath
            else:
                log.error(f"Unable to create {logtype} eventlog on {host}")
        except:
            log.error(f"Unable to gather winevent logs for {host}")

    def list_static_paths(self, host):
        static_filepaths = []
        for filepath in self.static_files:
            uncpath = os.path.join("\\\\", host, filepath.replace(":", "$"))
            static_filepaths.append(uncpath)
        return static_filepaths

    def is_eligible(self, filepath, startdate, enddate):
        modified_date = datetime.date.fromtimestamp(os.path.getmtime(filepath))
        if (modified_date >= startdate) and (modified_date <= enddate):
            log.debug(f"File is eligible: {filepath}")
            return True
        log.debug(
            f"File is not eligible : {filepath} : Allowed startdate : {startdate} : Allowed enddate : {enddate} : Filedate : {modified_date}"
        )
        return False


def main(startdate, enddate, configfile, outdir, traverse):
    config = Config(startdate, enddate, configfile, outdir)
    files = Files(startdate, enddate, config.logdirs, config.static_files)
    if traverse:
        files.traverse = True
    package = Package(config.outfile)

    pool = ThreadPool(5)
    all_logs = []
    for filepath in chain.from_iterable(
        pool.map(files.list_eligible_logfiles, config.hosts)
    ):
        all_logs.append(filepath)
    for filepath in chain.from_iterable(
        pool.map(files.list_static_paths, config.hosts)
    ):
        all_logs.append(filepath)
    if config.wineventlogs:
        for filepath in chain.from_iterable(
            pool.map(files.get_winevent_logpaths, config.hosts)
        ):
            all_logs.append(filepath)

    package.create_archive(all_logs)
    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("start_date", help="First log date range, format : YYYY-MM-DD")
    parser.add_argument("end_date", help="Last log date range, format : YYYY-MM-DD")
    parser.add_argument(
        "configfile",
        help="Configuration .ini file containing the list of hostnames and files to package",
    )
    parser.add_argument(
        "--recurse",
        action="store_true",
        default=False,
        help="If this flag is added, any directories will have all subdirectories traversed",
    )
    parser.add_argument(
        "--log-level",
        choices=["warn", "debug", "info"],
        default="error",
        help="Set logging level",
    )
    # parser.add_argument("--filter", help="Regex pattern filtering on eligible logs")
    # parser.add_argument("--arcname", default="LogPackage.zip", help="Name the output file")
    parser.add_argument(
        "--output-dir",
        default=".",
        help="Directory to place the log package, defaults to current working dir if not specified",
    )
    args = parser.parse_args()

    logging.basicConfig()
    log = logging.getLogger("LogPacker")
    log_level = {
        "warning": logging.WARNING,
        "info": logging.INFO,
        "debug": logging.DEBUG,
        "error": logging.ERROR,
    }
    log.setLevel(log_level[args.log_level])
    log.debug(f"Commandline arguments={args}\n")
    main(args.start_date, args.end_date, args.configfile, args.output_dir, args.recurse)
