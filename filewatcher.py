#!/usr/bin/env python3
"""Tool for watching a file, directory, or combination thereof, 
and exiting out when all conditions are met, or the defined timeout
has been exceeded.

"""
import argparse
import datetime
import logging
import os
import re
import time

from fnmatch import fnmatch


def is_path_pattern(path):
    """Determine if a given path is a 'patterned' path
    for use with fnmatch.

    """
    characters = ["[", "]", "{", "}", "?", "*", "+", "|", "^", "$"]
    for character in characters:
        if character in path:
            return True
    return False


def get_pattern_first_match(pattern):
    """Parse out the paths that match the fnmatch or
    regex patterns of the given containing path

    """
    working_path, pattern = os.path.split(pattern)
    if args.recurse:
        for root, subs, filenames in os.walk(working_path):
            for filename in filenames:
                fullpath = os.path.join(root, filename)
                log.debug(f"path={fullpath}, pattern={pattern}")
                if args.regex:
                    if re.match(pattern, fullpath) and is_eligible(
                        fullpath, args.day_limit
                    ):
                        return fullpath
                else:
                    if fnmatch(fullpath, pattern) and is_eligible(
                        fullpath, args.day_limit
                    ):
                        return fullpath
    else:
        if args.regex:
            for pathname in os.listdir(working_path):
                fullpath = os.path.join(working_path, pathname)
                log.debug(f"path={fullpath}, pattern={pattern}")
                if re.match(pattern, fullpath) and is_eligible(
                    fullpath, args.day_limit
                ):
                    return fullpath
        else:
            for pathname in os.listdir(working_path):
                fullpath = os.path.join(working_path, pathname)
                log.debug(f"path={fullpath}, pattern={pattern}")
                if fnmatch(fullpath, pattern) and is_eligible(fullpath, args.day_limit):
                    return fullpath
    return None


def is_eligible(path, day_limit):
    """Determine if a matching path is eligible to work
    with, by eliminating files with stale data or that are
    still being modified by another process.

    """
    if not os.path.isfile(path):
        # Since this is a filewatcher, files may not exist at the time.
        # Drop back to calling loop rather than failing out
        return False
    if not is_stable(path):
        return False

    filedate = datetime.date.fromtimestamp(os.path.getmtime(path))
    today = datetime.date.today()
    log.debug(f"path={path}, filedate={filedate}")
    if today - filedate <= datetime.timedelta(day_limit):
        return True
    else:
        return False


def is_stable(path):
    """Ensure that a file is not still in the process of being
    copied or written, by assuming that it is safe if
    not modified within a stability_time window.

    """
    last_modified = time.time() - os.path.getmtime(path)
    log.debug(f"path={path}, file mtime={last_modified} seconds ago")
    if last_modified > args.stability_time:
        return True
    else:
        return False


def create_msgin(msg, outpath, filematch):
    """Parses the input msg and rewrites it to include the file
    output path for processing by an external system.

    MSGIN format:
        $PROPERTY:ADD,<property name>,<initial value>,user,pass

    """
    fields = msg.split(",")
    fields[2] = filematch
    msgout = ",".join(fields)
    if outpath is None:
        outpath = os.getcwd()
    msgfile = os.path.join(outpath, f"filewatcher_{time.time()}.msgin")

    log.debug(f"Variables msg={msg}, outpath={outpath}, filematch={filematch}")
    with open(msgfile, "w") as outfile:
        log.info(f"Writing {msgout} to {msgfile}")
        outfile.write(msgout)
    return None


def main(args):
    rcode = 0
    start_time = time.time()
    timeout_seconds = args.timeout * 60
    filematch = None

    while True:
        runtime = time.time() - start_time
        if runtime >= timeout_seconds:
            log.error(
                f"Unable to find matches in the configured timeout period ({args.timeout} minutes)"
            )
            log.error(f"Unable to find a match for: {args.path}")
            rcode = 1
            return rcode

        if is_path_pattern(args.path):
            filematch = get_pattern_first_match(args.path)
        else:
            if is_eligible(args.path, args.day_limit):
                filematch = args.path

        if filematch:
            print(f"Found file {filematch}")
            if args.msgin:
                create_msgin(args.msgin, args.msgin_path, filematch)
            return rcode
        time.sleep(5)  # Avoid slamming resources with file-checks


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        """Watch file(s) and/or directories for an inputed set of criteria.
        If the criteria is met, exit out normally. If the criteria is not
        met in the defined timeout period, exit out with an error.
        
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "path",
        help="Path or pattern to watch for file matches. If using a pattern, enclose it in quotes to avoid shell expansion issues.",
    )
    parser.add_argument(
        "--timeout",
        type=int,
        default=60,
        help="Time to continue watching before giving up and returning failure, in minutes",
    )
    parser.add_argument(
        "--day-limit",
        type=int,
        default=0,
        help="How many days back an eligible matching file can be (by modified date).",
    )
    parser.add_argument(
        "--msgin",
        type=str,
        help="MSGIN comma separated string, including the 'empty fields'. Enclose in quotes to avoid issues with the shell",
    )
    parser.add_argument(
        "--msgin-path",
        type=str,
        help="Path of MSGIN folder where the file will be written for pickup by the external system for processing. Defaults to current working directory.",
    )
    parser.add_argument(
        "--stability-time",
        type=int,
        default=300,
        help="Seconds that a file must have not been modifed within, to avoid picking up files mid-creation/copy.",
    )
    parser.add_argument(
        "--recurse",
        action="store_true",
        default=False,
        help="Enable searching through subdirectories for a match.",
    )
    parser.add_argument(
        "--regex",
        action="store_true",
        default=False,
        help="If using a file pattern, use regex rules instead of shell file-matching.",
    )
    parser.add_argument(
        "-l",
        "--log-level",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
        type=str.upper,
        default="WARNING",
        help="Include logging for everything up to the level selected",
    )
    args = parser.parse_args()

    logging.basicConfig()
    log = logging.getLogger(__name__)
    log.setLevel(getattr(logging, args.log_level))

    log.debug(f"Variables used:{vars(args)}")
    main(args)
