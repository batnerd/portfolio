"""Module to quickly report errors or other things via email, using
the common settings at this time.

"""
import smtplib
from email.mime.text import MIMEText


_SMSGATEWAYS = (
    "tmomail.net",  # T-Mobile Main
    "vtext.com",  # Verizon SMS, NO MMS
    "txt.att.net",  # AT&T SMS
    "sms.myboostmobile.com",  # Boost Mobile
)


def email_message(fromAddr, toAddr, strSubject, strMsg):
    """Take a message and report it to an e-mail address, using the internal
    email server

    """
    msg = MIMEText(strMsg)
    msg["subject"] = strSubject
    postoffice = smtplib.SMTP()

    postoffice.connect("postoffice", 25)
    postoffice.ehlo()
    postoffice.sendmail(
        fromAddr,
        [
            toAddr,
        ],
        msg.as_string(),
    )
    return None


def gmail_message(fromAddr, toAddr, password, strSubject, strMsg):
    """Use gmail to send an email.

    fromAddr = Also used as the username, since gmail accts are based on the
        address these days (ie. username IS acct)

    """
    msg = MIMEText(strMsg)
    msg["subject"] = strSubject
    gmail = smtplib.SMTP("smtp.gmail.com:587")

    gmail.starttls()
    gmail.ehlo()
    gmail.login(fromAddr, password)
    gmail.sendmail(fromAddr, toAddr, msg.as_string())
    gmail.quit()
    return None


def sms_message(fromEmail, password, number, network, subject, message):
    """This function uses the well known sms-to-email services
    that many of the primary telecoms offer, as well as plenty of
    other services to allow.

    Note that some networks will block you if you do not have a
        registered IP address that you are sending from.

    """
    toAddr = "@".join((number, network))
    msg = MIMEText(message)
    msg["subject"] = subject
    gmail = smtplib.SMTP("smtp.gmail.com:587")

    gmail.starttls()
    gmail.ehlo()
    gmail.login(fromEmail, password)
    gmail.sendmail(fromEmail, toAddr, msg.as_string())
    gmail.quit()
    raise None
