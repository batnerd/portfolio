#!/usr/bin/env python3
"""Module to automate the daily maintenance checks and
report on statuses for things that cannot be fixed
automatically (wmic start service, etc).

"""
import argparse
import json
import subprocess
import re
import smtplib

from email.mime.text import MIMEText


_HOSTS = []


def read_config(filepath):
    with open(filepath, "r") as infile:
        return json.loads(infile.read())


class WindowsHost:
    def __init__(self, host):
        self.hostname = host
        self.services = {}
        self.status = ""  # ERROR, WARN, OK
        return None

    def get_services(self):
        """Call WMIC to gather up all running services in a dictionary"""
        re_services = r"([\w\W\s]+?)\r\r\n"
        cmd = [
            "wmic",
            "/node:" + '"' + self.hostname + '"',
            "service",
            "get",
            "caption,state",
        ]
        stdout, stderr = subprocess.Popen(
            " ".join(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True
        ).communicate()
        ascii_data = stdout.decode("ascii", errors="ignore")
        data = re.findall(re_services, ascii_data, re.DOTALL)
        for service in data:
            key, value = service.strip().rsplit(" ", 1)
            self.services[key.strip()] = value.strip()
        return None

    def is_service_running(self, servicename):
        if servicename in self.services:
            if self.services[servicename] == "Running":
                self.status = "OK"
                return True
            else:
                return False
        # If service not found, throw error report so user can correct asap
        self.status = "ERROR"

    def restart_service(self, servicename):
        cmd = [
            "wmic",
            "/node:" + '"' + self.hostname + '"',
            "service",
            "where",
            "caption=" + '"' + servicename + '"',
            "call",
            "startservice",
        ]
        stdout, stderr = subprocess.Popen(
            " ".join(cmd), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        ).communicate()

        # Windows seems to always return zero, but generate stdout with text
        # return value. Parse text for real value
        re_retval = r"ReturnValue = ([\-\d]+)"
        try:
            retval = int(re.search(re_retval, stdout.decode("ascii")).group(1))
        except AttributeError:
            retval = 1
            self.status = "ERROR"

        # Don't reset host status to warn if a prior service failed to start!
        if retval == 0 and not self.status == "ERROR":
            self.status = "WARN"
        else:
            self.status = "ERROR"


class Reporting:
    def __init__(self):
        self.data = []
        self.reportable = False  # Configure to true if warning or errors in hosts
        return None

    def email(self, fromAddr, toAddr, strSubject, strMsg):
        """Take a message and report it to an e-mail address,
        using the office365 email smtp address

        """
        msg = MIMEText(strMsg)
        msg["subject"] = strSubject
        postoffice = smtplib.SMTP()

        postoffice.connect("mailserver.company.com", 25)
        postoffice.ehlo()
        postoffice.sendmail(
            fromAddr,
            [
                toAddr,
            ],
            msg.as_string(),
        )
        return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--report", action="store_true", help="Force a report even if everything all OK"
    )
    parser.add_argument("--debug", action="store_true", help="Debugging mode")
    args = parser.parse_args()

    if args.debug:
        import sys

        # Debugging block here =)
        #
        #
        sys.exit(0)

    configfile = r"audit-checks-example.json"
    _HOSTS = read_config(configfile)

    # Run service checks, and report
    report = Reporting()
    for host in _HOSTS:
        report.data.append("\n\n" + host["hostname"])
        winhost = WindowsHost(host["hostname"])
        winhost.get_services()
        for service in host["services"]:
            if winhost.is_service_running(service):
                report.data.append("\n\t" + service + " : OK")
            else:
                winhost.restart_service(service)
                if winhost.status == "WARN":
                    report.data.append("\n\t" + service + " : WARN")
                elif winhost.status == "ERROR":
                    report.data.append("\n\t" + service + " : ERROR!!")
                report.reportable = True

    email_body = " ".join(report.data)
    if report.reportable or args.report:
        report.email(
            "Alerts@company.com", "tech@company.com", "Server Statuses", email_body
        )
    else:
        pass
