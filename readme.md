## Sample Scripts

This repo contains code that I have found useful at various 
places to facilitate automatic workflows and processes around 
administrative tasks.

**archive.py**

- A tool for "cold storage" type archival, as part of larger research 
processes. This was created to solve a storage issue on performant,
low storage availability servers, moving the data after a given amount 
of time to larger storage locations. Packages the data into Monthly 
archive files to keep the amount of data manageable for later supporting
research / tools.

**audit-checks.py**

- Script that was created to assist with critical service monitoring where 
a full featured monitoring system is unavailable. The script will also attempt 
to restart a service. The main reasoning for this script was to handle an issue 
where the vendor took an extended amount of time to research and correct 
an issue with the app crashing on a critical service, leaving the admins to have
to manually check the service throughout the day (or get reports from users.). 
Not a long term solution, but helps bridge the gap between getting a service 
running, and getting the root cause corrected. 
Associated **audit-checks-examlple.json** is the corresponding expected
config file input format.

**batchreboot.py**

- Tool for "rolling" servers after updates or maintenance schedules. The reasoning
for this tool was to reduce system resource usage (such as I/O in this case) that 
could cause errors within VM's that were competing for hypervisor resources.

**certwatch.py**

- Script that polls a list of hostname:port pairs to determine if the SSL certificate 
is expiring within a given number of days. When expiring certificates 
are found, prints them to output and returns exit code 2 for possible use in orchestration
calling system logic. Shows error and returns code 1 when it failing 
to get cert data for a hostname. Assumes standard port 443 if not given in pair.

**filefeed.py**

- Tool that copies files from one location to another, in a controlled way. This has
been used to keep file ingestion utilities from getting overwhelmed by processes that 
generate many more files than it is able to process in a timely manner, keeping other
ingestion processes from running while the first one attempts to keep up, unsuccessfully.

**filewatcher.py**

- A recreation of an orchestration system tool that was limited to file
created-date. Many processes will overwrite an existing file, leaving
the creation date unchanged. This caused problems with "stale" data calculations.
This tool uses the file modified-date as an alternative, more accurate estimation 
of file data age.

**get-hdd-ages.py**

- Legacy tool I used to gather up the power on age of hard drives on a system where I 
had no monitoring tools. This would allow us to identify hardware that was coming to 
the end of the service life, and would need to be replaced proactively before failure.

**inventory.py**

- One of my oldest tools, used for gathering up workstation information on large sites
as part of hardware audits. Allows the user to input a list of hostnames / IPs, and 
loop over the listing for a given amount of time, getting WMIC responses to parse into
a .csv file containing machine information.

**logtool.py**

- A script created to facilitate working with vendors during troubleshooting of many 
interconnected servers. Many times the process would be slowed by different techs 
asking for differing log files from various servers, depending on the situation. This 
tool will allow the user to specify all interesting logs and configuration files to 
gather from one server or many, and packages them all into one cohesive .zip file 
that can be sent with everything the vendor would need to troubleshoot, reducing the 
back-and-forth between requests. The **logtool.ini** file is the format of the configuration 
file to be used in this process.

**mail.py**

- Micro script to allow other scripts or processes to send an alert message via 
email to users. Basically a form of micro-alerting capability.

**purge.py**

- Tool used for cleanup of processes that do not clean up after themselves. I have 
had to support processes that would log to disk without any sort of rotation or 
retention, until the disk would fill up and cause even larger problems. Also has worked 
very well with applications that create temp files as part of the workflow, but does
not always cleanly remove them afterward, causing the same storage capacity problems.

**template-script.py**

- Probably one of the most useful things to come out of my repo, this template
is something I use to start any 'new' scripts as boilerplate. This keeps 
a quick and consistent way to get logging and arguments configured


## Saltstack states

This repo also contains the saltstack state template I am using
to create conistent states across various applications. This 
should help in the "cattle not pets" sysadmin methodology. Having
the consistency and layering of the states helps with supporting
code having a stable API equivalent.
